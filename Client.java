package rohan;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client
{
	public final static int DEF_PORT=9;
	public final static int MAX_SIZE=65507;
	public static void main(String arg[])
	{  
		try{       	

			InetAddress server=InetAddress.getByName("127.0.0.1");
			Socket soc = new Socket(server, 8022);	
			System.out.println("enter filename to request from registry");
			Scanner sc=new Scanner(System.in);
			String filename=sc.next();
			
			 OutputStream os=soc.getOutputStream();
			 DataOutputStream dos=new DataOutputStream(os);
			
			 dos.writeUTF(filename);			
			 
			ObjectInputStream ois = new ObjectInputStream(soc.getInputStream());
			byte[] buffer1 = (byte[])ois.readObject();
			
			FileOutputStream fos = new FileOutputStream("\\D:\\Client\\encryptedFile.wsdl");
			fos.write(buffer1);
			
			fos.close();
			
			String str=(String) Decrypt.decrypto("\\D:\\Client\\encryptedFile.wsdl");
			
			new File("\\D:\\Client\\Service.wsdl");
			FileWriter fw = new FileWriter("\\D:\\Client\\Service.wsdl");
			System.out.println("Received file is decrypted :)");
			fw.write(str);
			fw.close();
			soc.close();
			sc.close();
			dos.close();
			ois.close();

		
		}
		catch(Exception e)
		{
			System.out.println("Error : "+e);
		}
	}
	 
}